OpenLayers Maven Artifact
-------------------------

The [OpenLayers JavaScript library][1] packaged as a [Maven][2] artifact 
suitable for the [JavaScript Maven Plugin][3].

Contains the minimized and the uncompressed JavaScript code of the OpenLayers
library and some incomplete closure compiler externs.

[1]: http://openlayers.org/ "OpenLayers JavaScript Library"
[2]: http://maven.apache.org/ "Apache Maven"
[3]: https://github.com/kayahr/javascript-maven-plugin "JavaScript Maven Plugin"
