/**
 * @type {Object}
 */
var OpenLayers = {};

/**
 * @constructor
 */
OpenLayers.Geometry = function() {};

/**
 * @param {number} x
 * @param {number} y
 * @constructor
 * @extends {OpenLayers.Geometry}
 */
OpenLayers.Geometry.Point = function(x, y) {};

/**
 * @param {Array.<OpenLayers.Geometry.Point>} points
 * @constructor
 * @extends {OpenLayers.Geometry}
 */
OpenLayers.Geometry.LineString = function(points) {};

/**
 * @param {string} projCode
 * @param {Object=} options
 * @constructor
 */
OpenLayers.Projection = function(projCode, options) {};

/**
 * @param {number} lon
 * @param {number} lat
 * @constructor
 */
OpenLayers.LonLat = function(lon, lat) {};

/**
 * @type {number}
 */
OpenLayers.LonLat.prototype.lon;

/**
 * @type {number}
 */
OpenLayers.LonLat.prototype.lat;

/**
 * @param {OpenLayers.Projection} source
 * @param {OpenLayers.Projection} dest
 */
OpenLayers.LonLat.prototype.transform = function(source, dest) {};

/**
 * @param {number} w
 * @param {number} h
 * @constructor
 */
OpenLayers.Size = function(w, h) {};

/**
 * @param {number} x
 * @param {number} y
 * @constructor
 */
OpenLayers.Pixel = function(x, y) {};

/**
 * @param {Object} object
 * @param {Element} element
 * @param {Array.<string>} eventTypes
 * @param {boolean} fallThrough
 * @param {Object} options
 * @constructor
 */
OpenLayers.Events = function(object, element, eventTypes, fallThrough, options) {};

/**
 * @param {string} type
 * @param {Object} obj
 * @param {Function} func
 * @param {(boolean|Object)=} priority
 */
OpenLayers.Events.prototype.register = function(type, obj, func, priority) {};

/**
 * @param {string} url
 * @param {OpenLayers.Size} size
 * @param {?OpenLayers.Pixel} offset
 * @param {?Function=} calculateOffset
 * @constructor
 */
OpenLayers.Icon = function(url, size, offset, calculateOffset) {};

/**
 * @param {Element|string} div
 * @param {Object=} options
 * @constructor
 */
OpenLayers.Map = function(div, options) {};

/**
 * @type {OpenLayers.Events}
 */
OpenLayers.Map.prototype.events;

/**
 * @param {OpenLayers.Layer} layer
 */
OpenLayers.Map.prototype.addLayer = function(layer) {};

/**
 * @param {OpenLayers.LonLat|Array} lonlat
 * @param {number} zoom
 * @param {boolean=} dragging
 * @param {boolean=} forceZoomChange
 */
OpenLayers.Map.prototype.setCenter = function(lonlat, zoom, dragging, forceZoomChange) {};

/**
 * @return {OpenLayers.LonLat}
 */
OpenLayers.Map.prototype.getCenter = function() {};

/**
 *
OpenLayers.Map.prototype.setZoom = function() {};

/**
 * @return {number}
 */
OpenLayers.Map.prototype.getZoom = function() {};

/**
 * @param {number} zoom
 */
OpenLayers.Map.prototype.zoomTo = function(zoom) {};

/**
 * @param {string} name
 * @param {Object=} options
 * @constructor
 */
OpenLayers.Layer = function(name, options) {};

/**
 * @param {string} name
 * @param {Object=} options
 * @constructor
 * @extends {OpenLayers.Layer}
 */
OpenLayers.Layer.Markers = function(name, options) {};

/**
 * @param {OpenLayers.Marker} marker
 */
OpenLayers.Layer.Markers.prototype.addMarker = function(marker) {};

/**
 * @param {OpenLayers.Marker} marker
 */
OpenLayers.Layer.Markers.prototype.removeMarker = function(marker) {};

/**
 * @param {string} name
 * @param {Object=} options
 * @constructor
 * @extends {OpenLayers.Layer}
 */
OpenLayers.Layer.Vector = function(name, options) {};

/**
 * @param {Array.<OpenLayers.Feature.Vector>} features
 * @param {Object=} options
 */
OpenLayers.Layer.Vector.prototype.addFeatures = function(features, options) {};

/**
 * @param {Array.<OpenLayers.Feature.Vector>} features
 * @param {Object=} options
 */
OpenLayers.Layer.Vector.prototype.removeFeatures = function(features, options) {};

/**
 * @param {?string} name
 * @param {?string} url
 * @param {Object=} options
 * @constructor
 * @extends {OpenLayers.Layer}
 */
OpenLayers.Layer.OSM = function(name, url, options) {};

/**
 * @param {OpenLayers.Layer} layer
 * @param {OpenLayers.LonLat} lonlat
 * @param {Object} data
 * @constructor
 */
OpenLayers.Feature = function(layer, lonlat, data) {};

/**
 * @param {OpenLayers.Geometry} geometry
 * @param {?Object=} attributes
 * @param {?Object=} style
 * @constructor
 */
OpenLayers.Feature.Vector = function(geometry, attributes, style) {};

/**
 * @param {OpenLayers.LonLat} lonlat
 * @param {OpenLayers.Icon} icon
 * @constructor
 */
OpenLayers.Marker = function(lonlat, icon) {};

/**
 */
OpenLayers.Marker.prototype.destroy = function() {};

/**
 * @param {Object=} options
 * @constructor
 */
OpenLayers.Control = function(options) {};

/**
 * @param {Object=} options
 * @constructor
 */
OpenLayers.Control.Navigation = function(options) {};
